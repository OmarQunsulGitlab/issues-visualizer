require 'httparty'
require 'json'
require 'erb'
require 'date'

@group_id = 9970
@epic_id = 12383

@headers = {"PRIVATE-TOKEN": ARGV[0]}
@data = {
  root_id: @epic_id,
  issues: {},
  epics: {},
  milestones: {}
}

def epic_issues(epic_id)
  response = HTTParty.get("https://gitlab.com/api/v4/groups/#{@group_id}/epics/#{epic_id}/issues/?per_page=50", headers: @headers)
  issues = JSON.parse(response.body)
  issues = issues.map do |issue|
    i = issue.slice("id", "iid", "title", "web_url", "milestone", "epic", "state", "weight", "assignees", "labels")

    i["labels"] = i["labels"].grep /workflow/

    i
  end
end

# state: opened, closed
# milestone { title: "..." }
# assignees [{name: ...}]

def epic_epics(epic_id)
  response = HTTParty.get("https://gitlab.com/api/v4/groups/#{@group_id}/epics/#{epic_id}/epics/?per_page=50", headers: @headers)
  epics = JSON.parse(response.body)
  epics = epics.map{|epic| epic.slice("id", "iid", "title", "web_url", "parent_iid") }
end

def traverse_epic(epic_id, path, level = 0)
  issues = epic_issues(epic_id)

  # TODO: Load Root Epic if level == 0

	issues.each do |issue|
    issue["path"] = path + [epic_id]
    @data[:issues][issue["iid"]] = issue

    milestone = issue["milestone"]
    next if milestone.nil?
		@data[:milestones][milestone["iid"]] ||= issue["milestone"]
	end

  epics = epic_epics(epic_id)

  epics.each do |epic|
    @data[:epics][epic['iid']] = epic
    traverse_epic(epic['iid'], path + [epic_id], level + 1)
  end
end

traverse_epic(@epic_id, [])

@data[:milestones_sorted] = @data[:milestones].values.sort{|a, b| a["title"] <=> b["title"] }

# Populate miletones data
@data[:milestones].values.each do |milestone|
  url = "https://gitlab.com/api/v4/groups/#{@group_id}/milestones?iids[]=#{milestone['iid']}"
  response = HTTParty.get(url, headers: @headers)
  milestone_data = JSON.parse(response.body)[0]
  next if milestone_data.nil?
  start_date = milestone_data["start_date"]
  due_date = milestone_data["due_date"]
  milestone["start_date"] = start_date
  milestone["due_date"] = due_date

  next if start_date.nil? || due_date.nil?

  milestone["current"] = Date.parse(start_date) <= Time.now.to_date && Date.parse(due_date) >= Time.now.to_date
end

template = File.read('./template.html.erb')
output = ERB.new(template).result(binding)
f = File.new("index.html", 'w')
f.write(output)
f.close

